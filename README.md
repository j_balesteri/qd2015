# QUMANI DESIGNS 2015 #

###  Overview ###

Build out the next iteration of Qumani Designs to launch prior to Dec 31 2014. Concentration is on speed, ease of use, and 'elegant simplicity'. 


### Objectives ###

* Build out new version of Qumani Designs
* Utilize a more 'Flat UI'

### Initial Stack ###

* Angular
* 'Vanilla' JS
* Bootstrap (minimal usage of built ins)
* SASS

### Stack Evolution ###
* Incorporate:
      * React.js
      * Polymer
      * D3 (charts and graphs for statistics)

###Site Made Live ###

http://qumani.com