'use strict';


function NavigationController($scope, $location) {
  $scope.topLinks = [
    { text: 'About', page: '/about' },
    { text: 'Services', page: '/services'},
    { text: 'Portfolio', page: '/portfolio'},
    { text: 'Contact', page: '/contact'}
  ];
  $scope.isActive = function (viewLocation) {
    return viewLocation === $location.path();
  };
}

new NavigationController();
