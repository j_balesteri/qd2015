'use strict';

/**
 * @ngdoc function
 * @name qd2015App.controller:PortfolioCtrl
 * @description
 * # PortfolioCtrl
 * Controller of the qd2015App
 */
var app = angular.module('qd2015App');

app.controller('PortfolioCtrl', function ($scope) {
  $scope.title = 'Portfolio';
  $scope.leadParagraph = 'Please feel free to view some previous projects below. The projects range in type and complexity. They are arranged in chronological order and have icons specifying the technology utilized in each.';
  $scope.heroImage = 'images/banners/portfolio-banner.svg';
  $scope.portfolioSections = [
    {
      title: 'Web Projects',
      content: 'Web development is the primary focus of Qumani Designs and we work with the latest advancements in technology. We constantly strive to keep up with not only current development technologies, but also design trends. Here are some of our most recent web projects. ',
      // individual projects
      projects: [
        {
          id: 'tSurgical',
          image: 'images/portfolio/web/thinkSurgical.jpg',
          title: 'Think Surgical',
          description: 'New website built with AngularJS, HTML5, CSS3, Javascript, and Bootstrap.',
          specialNotation: '*** Please note: no access to server to perform rewrites of angular "html5 mode"',
          year: '2014',
          month: '07',
          technologies: [
              'Responsive',
              'HTML5',
              'CSS3',
              'Javascript',
              'Angular',
              'Yeoman',
              'Grunt',
              'Bower',
              'Git',
              'SASS',
          ],
        },
        {
          id: 'gVape',
          image: 'images/portfolio/web/gVape.jpg',
          title: 'Guerrilla Vape',
          description: 'This responsive site was built using HTML5, CSS3, Javascript, and Zurb Foundation responsive framework. The project was built on top of Magento Community Edition as an eCommerce platform',
          year: '2014',
          month: '03',
          technologies: [
              'Responsive',
              'HTML5',
              'CSS3',
              'Javascript',
              'Magento',
              'Git',
              'SASS'
          ],
        },
        {
          id: 'qd2014',
          image: 'images/portfolio/web/qd2014.jpg',
          title: 'Qumani Designs - 2014',
          description: 'Quick iteration of website that was never intended for a long term solution. It was a responsive site built with HTML5, CSS3, Javascript, PHP, and Foundation.',
          year: '2013',
          month: '12',
          technologies: [
              'Responsive',
              'HTML5',
              'CSS3',
              'Javascript',
              'Git',
              'PHP',
              'SASS',
          ],
        },
        {
          id: 'xcm',
          image: 'images/portfolio/web/xcm.jpg',
          title: 'Xerox Content Management',
          description: 'This responsive site was built using HTML5, CSS3, Javascript, and Zurb Foundation responsive framework. The project was built on top of Wordpress as a content management system.',
          year: '2013',
          month: '12',
          technologies: [
              'Responsive',
              'HTML5',
              'CSS3',
              'Javascript',
              'Git',
              'Wordpress',
              'SASS',
          ],
        },
        {
          id: 'jive',
          image: 'images/portfolio/web/jive.jpg',
          title: 'Jive Software',
          description: 'This responsive site was built using HTML5, CSS3, Javascript, and Zurb Foundation responsive framework. The project was built on top of Wordpress as a content management system.',
          year: '2013',
          month: '03',
          technologies: [
              'Responsive',
              'HTML5',
              'CSS3',
              'Javascript',
              'Git',
              'Wordpress',
              'SASS',
          ],
        },
        {
          id: 'qd2013',
          image: 'images/portfolio/web/qd2013.jpg',
          title: 'Qumani Designs - 2013',
          description: 'This responsive site was built using HTML5, CSS3, Javascript, and Zurb Foundation responsive framework.',
          year: '2013',
          month: '01',
          technologies: [
              'Responsive',
              'HTML5',
              'CSS3',
              'Javascript',
              'Git',
          ],
        },
        {
          id: 'gGear',
          image: 'images/portfolio/web/gGear.jpg',
          title: 'Guerrilla Gear',
          description: 'This responsive site was built using HTML5, CSS3, Javascript, and Zurb Foundation responsive framework. The project was built using Wordpress as a content management system',
          year: '2014',
          month: '03',
          technologies: [
              'Responsive',
              'HTML5',
              'CSS3',
              'Javascript',
              'Wordpress',
              'Git',
              'SASS'
          ],
        },
      ] //projects
    },
    {
      title: 'Identity Projects',
      content: 'In addition to web development and design, Qumani Designs often handles more of the comprehensive design packages, including establishment of business identity. This is more than just a logo, it is also a branding guide, including fonts, color schemes, brand usage guidelines. However, often the logo is the largest single representation of branding. Here are some identity pieces built by Qumani Designs.',
      // individual projects
      projects: [
        {
          id: 'qdLogo2015',
          image: 'images/portfolio/identity/qdLogo2015.jpg',
          title: 'Qumani Designs Logo 2015',
          description: 'New logo for Qumani Designs.',
          year: '2014',
          month: '11',
          technologies: [
            'Illustrator'
          ],
        },
        {
          id: 'gVapeLogo',
          image: 'images/portfolio/identity/gVapeLogo.jpg',
          title: 'Guerrilla Vape Logo',
          description: 'Logo from Guerrilla Gear Clothing for eJuice manufacturing segment of business.',
          year: '2014',
          month: '01',
          technologies: [
            'Illustrator'
          ],
        },
        {
          id: 'abcLogo',
          image: 'images/portfolio/identity/abcLogo.jpg',
          title: 'Balesteri Construction Logo',
          description: 'Vector logo for Balesteri Construction.',
          year: '2009',
          month: '11',
          technologies: [
            'Illustrator'
          ],
        },
        {
          id: 'koffeeKinetics',
          image: 'images/portfolio/identity/kKineticsLogo.jpg',
          title: 'Koffee Kinetics Logo',
          description: 'Vector logo for a small business named Koffee Kinetics.',
          year: '2012',
          month: '06',
          technologies: [
            'Illustrator'
          ],
        },
        {
          id: 'nakedEye',
          image: 'images/portfolio/identity/nakedEyeLogo.jpg',
          title: 'Naked Eye Photography Logo',
          description: 'Vector logo for a small business named Naked Eye Photography.',
          year: '2010',
          month: '11',
          technologies: [
            'Illustrator',
            'Photoshop'
          ],
        },
        {
          id: 'gGearLogo',
          image: 'images/portfolio/identity/gGearLogo.jpg',
          title: 'Guerrilla Gear Logo',
          description: 'Logo built for small startup Guerrilla Gear Clothing.',
          year: '2011',
          month: '11',
          technologies: [
            'Illustrator'
          ],
        },
      ] //projects
    },
    {
      title: 'Print Projects',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      // individual projects
      projects: [
        {
          id: 'qdCards2015',
          image: 'images/portfolio/print/qdCards2015.jpg',
          title: 'Qumani Designs Cards for 2015',
          description: 'Business cards made in Q4 2014 for Qumani Designs',
          year: '2014',
          month: '10',
          technologies: [
            'Illustrator'
          ],
        },
        {
          id: 'gGearCards',
          image: 'images/portfolio/print/gGearCards.jpg',
          title: 'Guerrilla Gear Cards',
          description: 'Business cards made in 2013 for Guerrilla Gear.',
          year: '2013',
          month: '01',
          technologies: [
            'Illustrator'
          ],
        },
        {
          id: 'qdCards2012',
          image: 'images/portfolio/print/qd4Cards.jpg',
          title: 'Qumani Designs Cards 2012',
          description: 'Business cards made in 2012 for Qumani Designs',
          year: '2012',
          month: '06',
          technologies: [
            'Illustrator'
          ],
        },
        {
          id: 'abcCards',
          image: 'images/portfolio/print/abcCards.jpg',
          title: 'Balesteri Construction Cards',
          description: 'Business cards made for Balesteri Construction',
          year: '2009',
          month: '01',
          technologies: [
            'Illustrator'
          ],
        },
         {
          id: 'qdCards2009',
          image: 'images/portfolio/print/qd3Cards.jpg',
          title: 'Qumani Designs Cards 2009',
          description: 'Business cards made in 2009 for Qumani Designs',
          year: '2009',
          month: '01',
          technologies: [
            'Illustrator'
          ],
        },
      ] //projects
    },
    // {
    //   title: 'Misc Projects',
    //   content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    //   // individual projects
    //   projects: [
    //     {
    //       id: '***',
    //       image: 'http://placehold.it/640x360&text=Large+Image',
    //       title: 'Project Title',
    //       description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    //       year: '2014',
    //       month: '10',
    //       technologies: [

    //       ],
    //     },
    //   ] //projects
    // }
    ];

  });

