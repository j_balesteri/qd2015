'use strict';

/**
 * @ngdoc function
 * @name qd2015App.controller:ContactCtrl
 * @description
 * # ThankyouCtrl
 * Controller of the qd2015App
 */
var app = angular.module('qd2015App');

app.controller('ThankyouCtrl', function ($scope) {
  $scope.title = 'Thank You';
  $scope.heroImage = 'images/banners/contact-us.svg';
  $scope.leadParagraph =
    'Thank you for taking the time to reach out. Someone will get back to you as quickly as possible. Have a great day!';
  $scope.pageContent = 'Feel free to continue to look around our site.'
  });
