'use strict';

/**
 * @ngdoc function
 * @name qd2015App.controller:ServicesCtrl
 * @description
 * # ServicesCtrl
 * Controller of the qd2015App
 */
var app = angular.module('qd2015App');

app.controller('ServicesCtrl', function ($scope) {
  $scope.title = 'Services';
  $scope.heroImage = 'images/banners/services-banner.svg';
  $scope.leadParagraph =
    'Web development is the primary focus of Qumani Designs, however, it is not a limitation. In addition to development, we also offer services for web design, graphic design, identity documents, print collateral, and more. ';
  $scope.servicesSections = [
    {
      image: 'images/services/web-development.svg',
      title: 'Web Development',
      content: 'Whether it is a static site, content management systems, or a feature rich eCommerce site, we can find a solution. Everyone has different requirements for their project and we will tailor a solution to your specific needs. Working in a wide array of coding languages and technologies allows us to furnish a product that will allow you to deliver a powerful presentation.',
    },
    {
      image: 'images/services/web-design.svg',
      title: 'Web Design',
      content: 'Already have a development team but need a little help with design ideas or branding guides, we have you covered. Not all developers are good with the look and feel a site and prefer to concentrate on building out the project. We can work in tandem with your internal team members to ensure that the proper image is projected.',
    },
    {
      image: 'images/services/corp-identity.svg',
      title: 'Corporate Identity',
      content: 'Need help with branding? We have graphic designers that would love nothing more than to put together a beautiful collection to help establish, or update your business identity. Some of these items include: ',
      list: [
        'Logo',
        'Business Cards',
        'Branded Materials (flyers, pamphlets, folders)',
        'PowerPoint Slide Decks'
      ]
    },
    {
      image: 'images/services/graphic-design.svg',
      title: 'Graphic Design',
      content: 'Need a little help with some graphics? No problem. We can produce graphics for you in a wide variety of formats including: PDF, PNG, JPG, AI, TIFF or SVG. It doesn\'t matter if it is for use on the web or in a printed project. You let us know what you need and we will get you taken care of.',
    },

    ];
  });
