'use strict';

/**
 * @ngdoc function
 * @name qd2015App.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the qd2015App
 */
var app = angular.module('qd2015App');

app.controller('ContactCtrl', function ($scope) {
  $scope.title = 'Contact Us';
  $scope.heroImage = 'images/banners/contact-us.svg';
  $scope.leadParagraph =
    'Qumani Designs takes pride in a high level of customer service and support. One of the largest advantages of using a local developer is the access that you have. We are available via email, various chat clients, telephone, and social media. If you have questions, comments, or concerns, please do not hesitate to reach out.';
  $scope.contactSection = [
    {
      image: 'http://placehold.it/640x360&text=Large+Image',
      title: 'Get In Touch',
    }
    ];
  });
