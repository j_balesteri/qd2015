'use strict';

/**
 * @ngdoc function
 * @name qd2015App.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the qd2015App
 */

angular.module('qd2015App')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
