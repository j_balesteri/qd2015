'use strict';

/**
 * @ngdoc function
 * @name qd2015App.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the qd2015App
 */
var app = angular.module('qd2015App');

app.controller('AboutCtrl', function ($scope) {
  $scope.title = 'About Us';
  $scope.heroImage = 'images/banners/about-banner.svg';
  $scope.leadParagraph =
    'Since 1999 Qumani Designs has been the name under which I operate my freelance business, which has become more of an artistic collective. There are several developers and designers that I work with in order to expands our offerings.';
  $scope.aboutSections = [
    {
      image: 'images/about/open_book.svg',
      title: 'Some Background',
      content: 'Since 1999 Qumani Designs has been the name under which I operate my freelance business, which has become more of an artistic collective. There are several developers and designers that I work with in order to expand offerings. I began developing as Flash Developer, and in the mid 2000s, seen that Flash was becoming antiquated, while HTML Canvas was gaining traction as a viable alternative. It was at this point that I began with more traditional front end development.',
    },
    {
      image: 'images/about/tech_stacks.svg',
      title: 'Tech Stack',
      content: 'Web development technologies are constantly evolving, and staying ahead of the curve takes some dedication. The coding languages used in a project are referred to as the "tech stack" and can vary greatly depending on use case, requirements, and a number of other variables. We are constantly learning and evolving our "stack" to follow the most solid methods and technologies. Currently preferred stack consists of:',
      list: [
        'HTML 5',
        'CSS 3 (SASS)',
        'Javascript',
        'AngularJS'
      ]
    },
    {
      image: 'images/about/collaboration.svg',
      title: 'Other Developers',
      content: 'Working with other developers and designers enables offering a larger scope of services. There are only so many hours in a day, and one person can only accomplish so much. In order to ensure that projects are completed quickly and properly, there is sometimes the need to have some help. In these situations, I prefer to enlist the help of someone who is more prolific in the specific coding language or technologies.',
    },

    ];
  });
