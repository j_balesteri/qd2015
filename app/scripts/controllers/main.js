'use strict';

/**
 * @ngdoc function
 * @name qd2015App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the qd2015App
 */

var app = angular.module('qd2015App');

app.controller('MainCtrl', function ($scope) {
    $scope.title = 'Welcome';
    $scope.leadParagraph =
      'Thank you for visiting Qumani Designs. Specializing in custom web development solutions, from simple presentations to eCommerce and corporate sites. All projects are tailored to your specific needs, and focused on the future success of your project.';
    $scope.indexSections = [
      {
        image: 'images/about/who-are-we.svg',
        title: 'Who We Are',
        content: 'My name is Justin and I am a front end web developer. Since 1999 Qumani Designs has been the name under which I operate my freelance business. It has become more of an artistic collective with several developers and designers pooling talent in order to expands offerings.',
        button: 'About Us',
        buttonLink: '#/about'
      },
      {
        image: 'images/main/what-we-do.svg',
        title: 'What We Do',
        content: 'Web Development is our primary focus, however we often perform many other tasks as well.',
        services:
        [
          'Graphic Design',
          'Logo Design',
          'Branding',
          'Collateral Materials',
          'More...'
        ],
        button: 'Services',
        buttonLink: '#/services'
      },
      {
        image: 'images/main/how-we-do-it.svg',
        title: 'How We Do It',
        content: 'Web development is constantly evolving and we follow new design trends and technologies closely. By doing so we can offer powerful projects that offer the following:',
        services:
        [
          'Pleasant user experience',
          'Modern, and clean user interfaces',
          'Projects that are easier to maintain',
          'Latest features used by premium websites',
          'A polished and professional presentation'
        ],
        button: 'Portfolio',
        buttonLink: '#/portfolio'
      },
      ];
  });



